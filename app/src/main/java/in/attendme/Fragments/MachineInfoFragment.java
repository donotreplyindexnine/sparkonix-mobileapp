package in.attendme.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.attendme.MachineServiceHistory;
import in.attendme.ManualList;
import in.attendme.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MachineInfoFragment extends Fragment {

    private TextView SerialNumber, ManufacturerName, InstallationDate, LastServiceDate, ViewManuals, MachineQRCode, ViewMachineHistory, ViewHistory;
    private Button infoButton, ViewAMC;
    private int MY_PERMISSIONS_REQUEST_PHONE_CALL = 0;
    private LinearLayout InfoDialog, ViewAMCDialog;

    public MachineInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_machine_info, container, false);
        SerialNumber = (TextView) rootView.findViewById(R.id.SerialNumber);
        ManufacturerName = (TextView) rootView.findViewById(R.id.ManufacturerName);
        InstallationDate = (TextView) rootView.findViewById(R.id.InstallationDate);
        LastServiceDate = (TextView) rootView.findViewById(R.id.LastServiceDate);
        infoButton = (Button) rootView.findViewById(R.id.infoButton);
        ViewAMC = (Button) rootView.findViewById(R.id.ViewAMC);
        ViewManuals = (TextView) rootView.findViewById(R.id.ViewManuals);
        InfoDialog = (LinearLayout) rootView.findViewById(R.id.InfoDialog);
        ViewAMCDialog = (LinearLayout) rootView.findViewById(R.id.ViewAMCDialog);
        MachineQRCode = (TextView) rootView.findViewById(R.id.MachineQRCode);
        ViewMachineHistory = (TextView) rootView.findViewById(R.id.ViewMachineHistory);
        ViewHistory = (TextView) rootView.findViewById(R.id.ViewHistory);


        if (getActivity().getIntent().getExtras().getBoolean("AMC_Empty"))
        {
            ViewAMC.setVisibility(View.GONE);
        }


        setListeners();

        return rootView;
    }

    private void setListeners() {
        SerialNumber.setText(getActivity().getIntent().getExtras().getString("Serial_Number"));
        ManufacturerName.setText(getActivity().getIntent().getExtras().getString("Manufacturer_Name"));
        InstallationDate.setText(getActivity().getIntent().getExtras().getString("Installation_Date"));
        LastServiceDate.setText(getActivity().getIntent().getExtras().getString("Last_Service_Date"));
        MachineQRCode.setText(getActivity().getIntent().getExtras().getString("QR_Data"));

        ViewManuals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(getActivity(), ManualList.class);
                startActivity(browserIntent);
            }
        });

        ViewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MachineServiceHistory.class);
                startActivity(intent);
            }
        });


        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InfoDialog.performClick();
            }
        });

        InfoDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View promptview = inflater.inflate(R.layout.view_manufacturer, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setView(promptview);
                final AlertDialog dialog = builder.create();
                final TextView CompanyName, custSupportName, custSupportContact, custSupportEmail;
                Button okay;

                CompanyName = (TextView) promptview.findViewById(R.id.CompanyName);
                custSupportName = (TextView) promptview.findViewById(R.id.custSupportName);
                custSupportContact = (TextView) promptview.findViewById(R.id.custSupportContact);
                custSupportEmail = (TextView) promptview.findViewById(R.id.custSupportEmail);


                okay = (Button) promptview.findViewById(R.id.okay);
                okay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                /*builder.setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });*/


                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);

                CompanyName.setText(getActivity().getIntent().getExtras().getString("Manufacturer_Name"));
                custSupportName.setText(getActivity().getIntent().getExtras().getString("Man_Customer_Support_Name"));
                custSupportContact.setText(getActivity().getIntent().getExtras().getString("Man_Customer_Support_Phone"));
                custSupportContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                            callPhone(custSupportContact.getText().toString().trim());
                        }
                        else
                        {
                            if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_REQUEST_PHONE_CALL);

                            }
                            else
                            {
                                callPhone(custSupportContact.getText().toString().trim());
                            }
                        }
                    }
                });
                custSupportEmail.setText(getActivity().getIntent().getExtras().getString("Man_Customer_Support_Email"));
                custSupportEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent Email = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", custSupportEmail.getText().toString().trim(), null));
                        Email.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        Email.setType("*/*");
                        startActivity(Intent.createChooser(Email, "Send Email"));
                    }
                });
                dialog.show();
            }
        });

        ViewAMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewAMCDialog.performClick();
            }
        });

        ViewAMCDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View promptview = inflater.inflate(R.layout.view_amc, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(promptview);
                final AlertDialog dialog = builder.create();
                TextView curAmcType, curAmcStartDate, curAmcEndDate, curAmcStatus;
                Button okay;

                curAmcType = (TextView) promptview.findViewById(R.id.curAmcType);
                curAmcStartDate = (TextView) promptview.findViewById(R.id.curAmcStartDate);
                curAmcEndDate = (TextView) promptview.findViewById(R.id.curAmcEndDate);
                curAmcStatus = (TextView) promptview.findViewById(R.id.curAmcStatus);
                okay = (Button) promptview.findViewById(R.id.okay);

                okay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                /*builder.setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });*/


                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCancelable(false);

                curAmcType.setText(getActivity().getIntent().getExtras().getString("Cur_AMC_Type"));
                curAmcStartDate.setText(getActivity().getIntent().getExtras().getString("Cur_AMC_Start_Date"));
                curAmcEndDate.setText(getActivity().getIntent().getExtras().getString("Cur_AMC_End_Date"));
                curAmcStatus.setText(getActivity().getIntent().getExtras().getString("Cur_AMC_Status"));
                dialog.show();
            }
        });


                //

    }

    private void callPhone(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+ phone));
        startActivity(callIntent);
    }


}
