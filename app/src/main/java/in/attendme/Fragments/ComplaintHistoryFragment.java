package in.attendme.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.attendme.Adapter.ComplaintHistoryAdapter;
import in.attendme.Model.ComplaintHistoryListResponse;
import in.attendme.R;
import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintHistoryFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout refreshLayout;
    private ArrayList<ComplaintHistoryListResponse> historyList = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private TextView EmptyView;

    public ComplaintHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_complaint_history, container, false);
        sharedPreferences = getActivity().getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.ComplaintHistoryList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new ComplaintHistoryAdapter(historyList, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        progressDialog = new ProgressDialog(getActivity());
        EmptyView = (TextView) rootView.findViewById(R.id.EmptyView);
        //write Loading dialog here

        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark
        );

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EmptyView.setVisibility(View.GONE);
                ComplaintHistoryAPI();
            }
        });

        showProgressDialog();
        ComplaintHistoryAPI();

        return rootView;
    }


    private void ComplaintHistoryAPI() {

        RequestQueue queue = Volley.newRequestQueue(getActivity());


        GenericRequest<JsonArray> request = new GenericRequest<JsonArray>(Request.Method.GET, APIUrls.ComplaintHistoryURL(sharedPreferences.getLong(Constants.MACHINE_ID, 0)), JsonArray.class, null, new Response.Listener<JsonArray>() {
            @Override
            public void onResponse(JsonArray response) {
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                hideProgressDialog();
                Type listType = new TypeToken<ArrayList<ComplaintHistoryListResponse>>() {
                }.getType();
                List<ComplaintHistoryListResponse> list = new Gson().fromJson(response, listType);
                ArrayList<ComplaintHistoryListResponse> listTemp = new ArrayList<ComplaintHistoryListResponse>();


                if (historyList != null && historyList.size() > 0) {
                    for (ComplaintHistoryListResponse item : list) {
                        if (!historyList.contains(item)) {
                            listTemp.add(0, item);
                        }
                    }
                    listTemp.addAll(historyList);
                }
                historyList.clear();
                historyList.addAll(listTemp);

                for (int i = list.size() - 1; i >= 0; i--) {
                    if (!historyList.contains(list.get(i))) {
                        historyList.add(list.get(i));
                    }
                }

                if (historyList.isEmpty()) {
                    EmptyView.setVisibility(View.VISIBLE);
                }

                /*for (ComplaintHistoryListResponse item : list) {
                    if (!historyList.contains(item)) {
                        historyList.add(item);
                    }
                }*/
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                hideProgressDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showNetworkErrorDialog(getString(R.string.No_Internet));
                }

                else if (historyList.isEmpty()) {
                    EmptyView.setVisibility(View.VISIBLE);
                }

                else
                {
                    showErrorDialog();
                }



                //EmptyView.setVisibility(View.VISIBLE);
            }
        }, APIUrls.getDefaultHeaders("authorization", sharedPreferences.getString(Constants.TOKEN, null)));
        queue.add(request);
    }

    private void showNetworkErrorDialog(String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(string)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void hideProgressDialog() {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Error")
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        ComplaintHistoryAPI();
    }
}
