package in.attendme;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import in.attendme.Model.RegisterComplaintRequest;
import in.attendme.Model.RequestResult;
import in.attendme.Utils.Constants;
import in.attendme.Utils.DatabaseHandler;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

public class RegisterComplaint extends AppCompatActivity {

    private TextView RegsiterComplaintSerialNumber;
    private EditText ComplaintDescription;
    private Button Submit;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private ImageButton backButton;

    private Receiver receiver;
    private DatabaseHandler db;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_complaint);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.RegisterComplaint);
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        RegsiterComplaintSerialNumber = (TextView) findViewById(R.id.RegsiterComplaintSerialNumber);
        RegsiterComplaintSerialNumber.setText(getIntent().getExtras().getString("Serial_Number"));


        receiver = new Receiver();
        db = new DatabaseHandler(this);

        ComplaintDescription = (EditText) findViewById(R.id.ComplaintDescription);
        ComplaintDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    hideKeyboard(view);
                }
            }
        });
        progressDialog = new ProgressDialog(this);
        Submit = (Button) findViewById(R.id.Submit);
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(ComplaintDescription.getWindowToken(), 0);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterComplaint.this);
                builder.setTitle(R.string.confirmation);
                builder.setMessage(R.string.confirmationMessage);
                builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        RegisterComplaintAPI();
                    }
                });

                builder.setNegativeButton(R.string.Cancle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
                alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.RedColor));

            }
        });



    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void RegisterComplaintAPI() {
        RegisterComplaintRequest complaint = new RegisterComplaintRequest();
        complaint.setMachineId(sharedPreferences.getLong(Constants.MACHINE_ID,0));
        complaint.setDetails(ComplaintDescription.getText().toString());

        RequestQueue queue = Volley.newRequestQueue(RegisterComplaint.this);

        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        GenericRequest<RequestResult> genericRequest = new GenericRequest<>(Request.Method.POST, APIUrls.RegisterComplaint(), RequestResult.class, complaint, new Response.Listener<RequestResult>() {
            @Override
            public void onResponse(RequestResult response) {
                if (progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }

                showConfirmationDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                showErrorDialog();
            }
        },APIUrls.getDefaultHeaders("authorization",sharedPreferences.getString(Constants.TOKEN,null)));

        queue.add(genericRequest);
    }

    private void showErrorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.RegisterComplaintFailed)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void showConfirmationDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.RegisterComplaintSuccessful)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        editor.putInt(Constants.WhichFragment,1).apply();
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        setNotificationNumber();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        else if (id == R.id.action_notifications) {
            Intent intent = new Intent(this,NotificationList.class);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unRegister receiver
        RegisterComplaint.this.unregisterReceiver(receiver);
    }

    class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction =  intent.getAction();
            if(intentAction.equalsIgnoreCase(Constants.SHOW_NOTIFICATION)){
                setNotificationNumber();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // creating and register receiver
        IntentFilter cIntentFilter = new IntentFilter(Constants.SHOW_NOTIFICATION);
        RegisterComplaint.this.registerReceiver(receiver, cIntentFilter);
        if (menu != null)
        {
            setNotificationNumber();
        }
    }

    private void setNotificationNumber() {

        int getNotificationCount = db.getUnreadNotificationCount();

        if (getNotificationCount<10)
        {
            switch (getNotificationCount)
            {
                case 0:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications));
                    break;

                case 1:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_one));
                    break;

                case 2:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_two));
                    break;

                case 3:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_three));
                    break;

                case 4:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_four));
                    break;

                case 5:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_five));
                    break;

                case 6:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_six));
                    break;

                case 7:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_seven));
                    break;

                case 8:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_eight));
                    break;

                case 9:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_nine));
                    break;

            }
        }

        else
        {
            menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_nine_plus));
        }


    }

    @Override
    public void onBackPressed() {
        if (getIntent().getExtras()!= null)
        {
            editor.putInt(Constants.WhichFragment, getIntent().getExtras().getInt("fragment_state")).apply();
        }

        super.onBackPressed();
    }
}
