package in.attendme;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import in.attendme.Model.MachineInformationResponse;
import in.attendme.Utils.Constants;
import in.attendme.Utils.DatabaseHandler;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

public class QRcodeReadingActivity extends AppCompatActivity {

    private static final String TAG = "QRcodeReadingActivity";
    private Button button;
    private int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Receiver receiver;
    private DatabaseHandler db;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_reading);
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_action_exit);*/
        getSupportActionBar().setTitle(R.string.app_name);
        button = (Button) findViewById(R.id.readBarcode);

        receiver = new Receiver();
        db  = new DatabaseHandler(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                   scanMachine();
                }
                else
                {
                    if (ContextCompat.checkSelfPermission(QRcodeReadingActivity.this,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(QRcodeReadingActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                    else
                    {

                        scanMachine();
                    }
                }


            }
        });

        invalidateOptionsMenu();
    }

    private void scanMachine() {
        IntentIntegrator integrator = new IntentIntegrator(QRcodeReadingActivity.this);
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setTimeout(8000);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                getMachineInfoAPI(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getMachineInfoAPI(final String contents) {

        final ProgressDialog progress = new ProgressDialog(this);
        //progress.setTitle("Loading");
        progress.setMessage(getString(R.string.wait));
        progress.setCancelable(false);
        progress.show();

        RequestQueue queue = Volley.newRequestQueue(QRcodeReadingActivity.this);
        GenericRequest<MachineInformationResponse> request = new GenericRequest<>(Request.Method.GET, APIUrls.MachineInformationURL(contents), MachineInformationResponse.class, null, new Response.Listener<MachineInformationResponse>() {
            @Override
            public void onResponse(MachineInformationResponse response) {

                if (progress.isShowing())
                {
                    progress.dismiss();
                }
                boolean AMC_Empty = false;
                editor.putLong(Constants.MACHINE_ID,response.getMachineId()).apply();
                editor.putLong(Constants.MANUFACTURE_ID,response.getManufacturer().getId()).apply();
                editor.putString(Constants.MODEL_NUMBER, response.getModelNumber()).apply();
                editor.putInt(Constants.WhichFragment, 0).apply();
                Intent intent = new Intent(QRcodeReadingActivity.this, MachineInformation.class);
                intent.putExtra("Serial_Number", response.getSerialNumber());
                intent.putExtra("Manufacturer_Name",response.getManufacturer().getCompanyName());
                if (response.getInstallationDate() != 0) {
                    intent.putExtra("Installation_Date",response.getDate(response.getInstallationDate()));
                }
                else
                {
                    intent.putExtra("Installation_Date","");
                }

                if (response.getLastServiceDate() != 0)
                {
                    intent.putExtra("Last_Service_Date",response.getDate(response.getLastServiceDate()));
                }
                else
                {
                    intent.putExtra("Last_Service_Date","");
                }



                intent.putExtra("Man_Customer_Support_Name",response.getManufacturer().getCustSupportName());
                intent.putExtra("Man_Customer_Support_Phone",response.getManufacturer().getCustSupportPhone());
                intent.putExtra("Man_Customer_Support_Email",response.getManufacturer().getCustSupportEmail());

                // This is important
                /*if (response.getCurAmcType().equals("")||response.getCurAmcStartDate()==0||response.getCurAmcEndDate()==0||response.getCurAmcStatus().equals(""))
                {

                }
*/
                intent.putExtra("Cur_AMC_Type",response.getCurAmcType());
                if (response.getCurAmcStartDate()!= 0)
                {
                    intent.putExtra("Cur_AMC_Start_Date", response.getDate(response.getCurAmcStartDate()));
                }
                else
                {
                    intent.putExtra("Cur_AMC_Start_Date", "");
                }

                if (response.getCurAmcEndDate() != 0)
                {
                    intent.putExtra("Cur_AMC_End_Date",response.getDate(response.getCurAmcEndDate()));
                }
                else
                {
                    intent.putExtra("Cur_AMC_End_Date","");
                }

                intent.putExtra("Cur_AMC_Status", response.getCurAmcStatus());
               /* if ((response.getCurAmcType().equals("")) && response.getCurAmcStartDate() ==0
                        && response.getCurAmcEndDate() == 0 &&
                        ((response.getCurAmcStatus()).equals("")))
                {
                    AMC_Empty = true;
                }*/
                intent.putExtra("QR_Data", contents);
                Log.d(TAG,"QR code tag: "+ contents);
                intent.putExtra("AMC_Empty",AMC_Empty);
                startActivity(intent);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progress.isShowing())
                {
                    progress.dismiss();
                }

                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showNetworkErrorDialog(getString(R.string.No_Internet));
                }
                else
                {
                    showErrorDialog();
                }

            }
        },APIUrls.getDefaultHeaders("authorization",sharedPreferences.getString(Constants.TOKEN,null)));

        queue.add(request);
    }

    private void showNetworkErrorDialog(String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
       if(requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {

                // If request is cancelled, the result arrays are empty.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                scanMachine();
                //new IntentIntegrator(QRcodeReadingActivity.this).setOrientationLocked(true).setBeepEnabled(true).setTimeout(10000).initiateScan();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void showErrorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.no_qr)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        this.menu = menu;
        setNotificationNumber();
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            //onBackPressed();

            /*AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(R.string.ExitConfirmation).setPositiveButton(R.string.Okay, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            }).setNegativeButton(R.string.Cancle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //alertDialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();

            alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
            alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.RedColor));*/

            return true;


        }
        else if (id == R.id.action_notifications) {
            Intent intent = new Intent(this,NotificationList.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.ExitConfirmation).setPositiveButton(R.string.Okay, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        }).setNegativeButton(R.string.Cancle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //alertDialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
        alertDialog.getButton(alertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.RedColor));
        //super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unRegister receiver
        QRcodeReadingActivity.this.unregisterReceiver(receiver);
    }

    class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction =  intent.getAction();
            if(intentAction.equalsIgnoreCase(Constants.SHOW_NOTIFICATION)){
                setNotificationNumber();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // creating and register receiver
        IntentFilter cIntentFilter = new IntentFilter(Constants.SHOW_NOTIFICATION);
        QRcodeReadingActivity.this.registerReceiver(receiver, cIntentFilter);
        if (menu != null)
        {
            setNotificationNumber();
        }
    }

    private void setNotificationNumber() {

        int getNotificationCount = db.getUnreadNotificationCount();

        if (getNotificationCount<10)
        {
            switch (getNotificationCount)
            {
                case 0:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications);
                    break;

                case 1:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_one);
                    break;

                case 2:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_two);
                    break;

                case 3:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_three);
                    break;

                case 4:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_four);
                    break;

                case 5:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_five);
                    break;

                case 6:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_six);
                    break;

                case 7:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_seven);
                    break;

                case 8:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_eight);
                    break;

                case 9:

                    menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_nine);
                    break;

            }
        }

        else
        {
            menu.findItem(R.id.action_notifications).setIcon(R.mipmap.ic_notifications_nine_plus);
        }


    }
}
