package in.attendme;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.attendme.Adapter.ServiceHistoryAdapter;
import in.attendme.Model.ServiceHistory;
import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

public class MachineServiceHistory extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout refreshLayout;
    private ArrayList<ServiceHistory> serviceHistories = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private TextView EmptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.machine_service_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle(getString(R.string.ServiceHistory));
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.serviceHistoryList);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new ServiceHistoryAdapter(serviceHistories, this);
        mRecyclerView.setAdapter(mAdapter);
        progressDialog = new ProgressDialog(this);
        EmptyView = (TextView) findViewById(R.id.EmptyView);
        //write Loading dialog here

        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark
        );

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EmptyView.setVisibility(View.GONE);
                getServiceHistory();
            }
        });

        showProgressDialog();
        getServiceHistory();
        //dummyData();

    }

   /* private void dummyData() {

        ServiceHistory serviceHistory = new ServiceHistory();
        serviceHistory.setActionTaken("Faulty part is removed");
        serviceHistory.setAssignedToEmail("abc@example.com");
        serviceHistory.setAssignedToMobile("1234567890");
        serviceHistory.setAssignedToName("Sagar");
        serviceHistory.setCompanyId(123456);
        serviceHistory.setServicingAssignedDate(new Date());
        serviceHistory.setServicingDoneDate(new Date());
        serviceHistory.setDetails("Machine was not working properly because of the problem with some parts. Now replaced");
        serviceHistory.setMachineId(343434);

        for (int i = 10 - 1; i >= 0; i--) {
            serviceHistories.add(serviceHistory);
        }

        mAdapter.notifyDataSetChanged();
    }*/



    private void getServiceHistory() {

        RequestQueue queue = Volley.newRequestQueue(MachineServiceHistory.this);
        GenericRequest<JsonArray> historyGenericRequest = new GenericRequest<JsonArray>(Request.Method.GET,
                APIUrls.getMachineServiceHistory(sharedPreferences.getLong(Constants.MACHINE_ID, 0)),
                JsonArray.class, null, new Response.Listener<JsonArray>() {
            @Override
            public void onResponse(JsonArray response) {
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                hideProgressDialog();
                Type listType = new TypeToken<ArrayList<ServiceHistory>>() {}.getType();
                List<ServiceHistory> list = new Gson().fromJson(response, listType);



                /*if (list != null && list.size() > 0) {
                    for (ServiceHistory item : list) {
                        if (!historyList.contains(item)) {
                            listTemp.add(0, item);
                        }
                    }
                    listTemp.addAll(historyList);
                }
                historyList.clear();
                historyList.addAll(listTemp);*/

                for (int i = list.size() - 1; i >= 0; i--) {
                    if (!serviceHistories.contains(list.get(i))) {
                        serviceHistories.add(list.get(i));
                    }
                }

                if (serviceHistories.isEmpty()) {
                    EmptyView.setVisibility(View.VISIBLE);
                }

                /*for (ComplaintHistoryListResponse item : list) {
                    if (!historyList.contains(item)) {
                        historyList.add(item);
                    }
                }*/
                mAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }
                hideProgressDialog();

                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showNetworkErrorDialog(getString(R.string.No_Internet));
                }
                else
                {
                    showErrorDialog();
                }
            }
        }, APIUrls.getDefaultHeaders("authorization",sharedPreferences.getString(Constants.TOKEN,null)));

        queue.add(historyGenericRequest);
    }

    private void showNetworkErrorDialog(String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void hideProgressDialog() {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgressDialog() {
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error")
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getServiceHistory();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            editor.putInt(Constants.WhichFragment, 0).apply();
            onBackPressed();
        }

       /* else if (id == R.id.action_notifications) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


}

