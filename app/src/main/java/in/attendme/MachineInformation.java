package in.attendme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import in.attendme.Fragments.ComplaintHistoryFragment;
import in.attendme.Fragments.MachineInfoFragment;
import in.attendme.Utils.Constants;
import in.attendme.Utils.DatabaseHandler;

public class MachineInformation extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Button Submit;
    private ImageButton backButton;
    private TabLayout tabLayout;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Receiver receiver;
    private DatabaseHandler db;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_information);

        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.MachineInformation);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        Submit = (Button) findViewById(R.id.Submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MachineInformation.this, RegisterComplaint.class);
                intent.putExtra("Serial_Number",getIntent().getExtras().getString("Serial_Number"));
                intent.putExtra("fragment_state", mViewPager.getCurrentItem());
                startActivity(intent);
            }
        });
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        receiver = new Receiver();
        db  = new DatabaseHandler(this);

        if (savedInstanceState != null)
        {

            int position = sharedPreferences.getInt(Constants.WhichFragment, 0);
            mViewPager.setCurrentItem(position);
        }



        //backButton = (ImageButton) findViewById(R.id.backButton);

       /* backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        int position = tabLayout.getSelectedTabPosition();
        outState.putInt("Selected_Tab", position);
        super.onSaveInstanceState(outState);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        setNotificationNumber();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == android.R.id.home) {
            onBackPressed();
        }

        else if (id == R.id.action_notifications) {
            Intent intent = new Intent(this,NotificationList.class);
            intent.putExtra("fragment_state", mViewPager.getCurrentItem());
            startActivity(intent);
            return true;
        }

        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            intent.putExtra("fragment_state", mViewPager.getCurrentItem());
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        Fragment fragment = new Fragment();
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position)
            {
                case 0:
                    fragment =  new MachineInfoFragment();
                    break;
                case 1:
                    fragment =  new ComplaintHistoryFragment();
                    break;
                /*default:
                    return null;*/
            }
            return fragment;
            //return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "M/c Info";
                case 1:
                    return "Complaint History";

            }
            return null;
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // Do something with the contact here (bigger example below)
            }
        }
    }*/


    @Override
    protected void onStart() {
        super.onStart();
        int position = sharedPreferences.getInt(Constants.WhichFragment, 0);
        mViewPager.setCurrentItem(position);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unRegister receiver
        MachineInformation.this.unregisterReceiver(receiver);
    }

    class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction =  intent.getAction();
            if(intentAction.equalsIgnoreCase(Constants.SHOW_NOTIFICATION)){
                setNotificationNumber();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // creating and register receiver
        IntentFilter cIntentFilter = new IntentFilter(Constants.SHOW_NOTIFICATION);
        MachineInformation.this.registerReceiver(receiver, cIntentFilter);

        if (menu != null)
        {
            setNotificationNumber();
        }

    }

    private void setNotificationNumber() {

        int getNotificationCount = db.getUnreadNotificationCount();

        if (getNotificationCount<10)
        {
            switch (getNotificationCount)
            {
                case 0:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications));
                    break;

                case 1:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_one));
                    break;

                case 2:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_two));
                    break;

                case 3:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_three));
                    break;

                case 4:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_four));
                    break;

                case 5:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_five));
                    break;

                case 6:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_six));
                    break;

                case 7:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_seven));
                    break;

                case 8:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_eight));
                    break;

                case 9:

                    menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_nine));
                    break;

            }
        }

        else
        {
            menu.findItem(R.id.action_notifications).setIcon(getResources().getDrawable(R.mipmap.ic_notifications_nine_plus));
        }


    }


}
