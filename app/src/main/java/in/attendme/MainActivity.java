package in.attendme;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import in.attendme.Model.RequestResult;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button SendOTP;
    private EditText MobileNumber;
    private TextView OTPHint;
    private int MY_PERMISSIONS_RECEIVE_SMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);

        SendOTP = (Button) findViewById(R.id.SendOTP);
        MobileNumber = (EditText) findViewById(R.id.MobileNumber);
        MobileNumber.setFocusable(true);
        MobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(view);
                }
            }
        });
        OTPHint = (TextView) findViewById(R.id.OTPHint);


        SendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                    if (MobileNumber.getText().toString().equals(""))
                    {
                        showDialog("Valid Mobile Number!");
                    }
                    else
                    {
                        OTPRequestAPI();
                    }
                }
                else
                {
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.RECEIVE_SMS)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.RECEIVE_SMS},
                                MY_PERMISSIONS_RECEIVE_SMS);

                    }
                    else
                    {
                        if (MobileNumber.getText().toString().equals(""))
                        {
                            showDialog("Valid Mobile Number!");
                        }
                        else
                        {
                            OTPRequestAPI();
                        }
                    }
                }




            }
        });
    }

    private void OTPRequestAPI() {

        //API for Mobile Number check
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

        final String MobileEditText = MobileNumber.getText().toString();

        Log.d(TAG, MobileNumber.getText().toString());

        final ProgressDialog progress = new ProgressDialog(this);
        //progress.setTitle("Loading");
        progress.setMessage(getString(R.string.wait));
        progress.setCancelable(false);
        progress.show();

        GenericRequest<RequestResult> IsValidMobileNumber = new GenericRequest<RequestResult>(Request.Method.GET, APIUrls.OTPRequest(MobileEditText), RequestResult.class, null, new Response.Listener<RequestResult>() {
            @Override
            public void onResponse(RequestResult response) {

                if (progress.isShowing())
                {
                    progress.dismiss();
                }
                Log.d(TAG,"Success: "+response.toString());
                Intent i = new Intent(MainActivity.this, ValidateOTP.class);
                i.putExtra("Mobile_Number",MobileEditText);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progress.isShowing())
                {
                    progress.dismiss();
                }

                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showErrorDialog(getString(R.string.No_Internet));
                }
                else
                {
                    OTPHint.setTextColor(getResources().getColor(R.color.RedColor));
                    OTPHint.setText(R.string.Wrong_Number);
                }

                Log.d(TAG,"Error" +error.toString());

            }
        },null);

        queue.add(IsValidMobileNumber);

    }

    private void showDialog(String field)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enter "+ field)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void showErrorDialog(String string)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == android.R.id.home) {
            onBackPressed();
        }

        else if (id == R.id.action_notifications) {
            Intent intent = new Intent(this,NotificationList.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if(requestCode == MY_PERMISSIONS_RECEIVE_SMS) {

            // If request is cancelled, the result arrays are empty.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                if (MobileNumber.getText().toString().equals(""))
                {
                    showDialog("Valid Mobile Number!");
                }
                else
                {
                    OTPRequestAPI();
                }

            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}

