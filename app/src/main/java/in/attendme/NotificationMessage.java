package in.attendme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class NotificationMessage extends AppCompatActivity {

    private TextView title, message, description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.NotificationDetails));
        setContentView(R.layout.activity_notification_message);

        title = (TextView) findViewById(R.id.title);
        message = (TextView) findViewById(R.id.message);
        description = (TextView) findViewById(R.id.description);

        title.setText(getIntent().getExtras().getString("title"));
        message.setText(getIntent().getExtras().getString("message"));
        description.setText(getIntent().getExtras().getString("description"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
                onBackPressed();
            }

        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

       /* else if (id == R.id.action_notifications) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot())
        {
            Intent intent = new Intent(NotificationMessage.this, NotificationList.class );
            startActivity(intent);
            finish();

        }
        else {

            super.onBackPressed();
        }

    }
}
