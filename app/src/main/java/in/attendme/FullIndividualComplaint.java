package in.attendme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import in.attendme.Utils.Constants;

import static in.attendme.R.id.Attendant;
import static in.attendme.R.id.AttendedByContact;

public class FullIndividualComplaint extends AppCompatActivity {

    private TextView RegsiterComplaintSerialNumber, Status ,Manufacturer,Complaint, Date, ContactNumber, OperatorName, AttendedBy, AttendantByLabel,AttendantContact,AttendantContactLabel;
    private ImageButton backButton;
    private LinearLayout AttendantLayout, AttendedByContactLayout;
    private ScrollView scrollView;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_individual_complaint);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("issueNumber"));
        RegsiterComplaintSerialNumber = (TextView) findViewById(R.id.RegsiterComplaintSerialNumber);
        Status = (TextView) findViewById(R.id.ComplaintStatus);
        Manufacturer = (TextView) findViewById(R.id.Manufacturer);
        Complaint = (TextView) findViewById(R.id.Complaint);
        Date = (TextView) findViewById(R.id.Date);
        ContactNumber = (TextView) findViewById(R.id.ContactNumber);
        OperatorName = (TextView) findViewById(R.id.Name);

        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        //Attendant name
        AttendantLayout = (LinearLayout) findViewById(Attendant);
        AttendedBy = (TextView) findViewById(R.id.AttendedBy);
        AttendantByLabel = (TextView) findViewById(R.id.AttendedByLabel);

        //Attendant Contact
        AttendantContact = (TextView) findViewById(R.id.AttendantContact);
        AttendedByContactLayout = (LinearLayout) findViewById(AttendedByContact);
        AttendantContactLabel = (TextView) findViewById(R.id.AttendedByLabel);

        if (getIntent().getExtras().getString("status").equals("OPEN"))
        {

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.RedColor)));
            AttendantLayout.setVisibility(View.GONE);
            AttendedByContactLayout.setVisibility(View.GONE);

        }
        else if (getIntent().getExtras().getString("status").equals("CLOSED"))
        {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.FaintGreen)));
        }
        else if (getIntent().getExtras().getString("status").equals("ASSIGNED"))
        {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.Violet)));
            AttendantByLabel.setText(getString(R.string.assigned_to));
            AttendantContactLabel.setText(getString(R.string.assigned_to_contact_number));

        }

        RegsiterComplaintSerialNumber.setText(getIntent().getExtras().getString("machineSerialNumber"));
        Status.setText(getIntent().getExtras().getString("status"));
        Manufacturer.setText(getIntent().getExtras().getString("manufacturerName"));
        Complaint.setText(getIntent().getExtras().getString("complaintDetail"));
        Date.setText(getIntent().getExtras().getString("loggedDate"));
        ContactNumber.setText(getIntent().getExtras().getString("operatorContactNumber"));
        OperatorName.setText(getIntent().getExtras().getString("operatorName"));
        AttendedBy.setText(getIntent().getExtras().getString("attendedBy"));
        AttendantContact.setText(getIntent().getExtras().getString("attendedByContactNumber"));


        /*backButton = (ImageButton) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            if (getIntent().getExtras()!= null)
            {
                editor.putInt(Constants.WhichFragment, 1).apply();
            }
            onBackPressed();
        }
        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

       /* else if (id == R.id.action_notifications) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
