package in.attendme.Volley;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Copyright (c) mytrux 2016
 */
public class APIUrls {


    private static String BASE_URL = "http://ec2-35-154-145-168.ap-south-1.compute.amazonaws.com:8080";
    //private static String BASE_URL = "http://ec2-35-154-33-26.ap-south-1.compute.amazonaws.com:8080";
    //private static String BASE_URL = "http://192.168.1.119:8080";

    private static HashMap<String, String> defHeaders = new HashMap<>();

    static {
        //defHeaders.put("Content-Type", "application/json");
        //defHeaders.put("accept","application/vnd.dsc.v1+json");
    }



    public static HashMap<String, String> getDefaultHeaders(String authorization, String string) {
        string = string.concat(":");
        byte[] token = Base64.encode(string.getBytes(), Base64.DEFAULT);
        string = new String(token);
        string = string.replace("\n","");
        defHeaders.put(authorization, "Basic " + string);
        return defHeaders;
    }

    public static String OTPRequest(String mobileEditText) {
        return BASE_URL + "/api/phonedevices/otp/"+ mobileEditText;
    }

    public static String ValidateOTP() {
        return BASE_URL + "/api/phonedevices/otp/validate";
    }

    public static String MachineInformationURL(String contents)
    {
        return BASE_URL + "/api/machine/getbyqrcode/"+contents;
    }

    public static String ComplaintHistoryURL(Long machineID)
    {
        return BASE_URL + "/api/issues/machine/"+ machineID;
    }

    public static String IndividualComplaintFullDetail(Long aLong)
    {
        return BASE_URL + "/api/issue/getdetail/"+aLong;
    }

    public static String RegisterComplaint()
    {
        return BASE_URL + "/api/issues";
    }

    public static String TermsAndConditions()
    {
        return BASE_URL + "/api/termscondition";
    }

    public static String PrivacyPolicy()
    {
        return BASE_URL + "/api/privacypolicy";
    }

    public static String UpdateFcmToken()
    {
        return BASE_URL+ "/api/phonedevice/fcmtoken";
    }

    public static String getMachineServiceHistory(long aLong) {
        return BASE_URL + "/api/machineamcservicehistories/bymachine/" + aLong;
    }

    public static String getManualList(long aLong, String string) throws UnsupportedEncodingException {
        return BASE_URL + "/api/machinedocs/"+ aLong + "/"+ URLEncoder.encode(string,"UTF-8");
    }


    /*public static String LOGIN(String licenseNumber, String fleetId, String id, String token) {
        String encodedToken = null;

        try {
            encodedToken = URLEncoder.encode(token, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            encodedToken = token;
            e.printStackTrace();
        }

        return BASE_URL + "/mytrux/fleet/" + fleetId + "/driver/" + licenseNumber + "/authenticate/" + id + "/" + encodedToken;
    }

    public static String UPLOADLOCATION(Long jobVehicleDriverId) {
        return BASE_URL + "/mytrux/bookingtrack/" + jobVehicleDriverId;
    }

    public static String UPLOADPOD(Long jobVehicleDriverId) {
        return BASE_URL + "/mytrux/booking/uploadpod/" + jobVehicleDriverId;
    }

    public static String LOGOUTAPI() {
        return BASE_URL + "/mytrux/fleet/driver/logout";
    }


    public static String BOOKINGTRACKAPI(String bookingNo, String vehicleRegNo) {
        String enCodedBookingNo = null;
        try {
            enCodedBookingNo = URLEncoder.encode(bookingNo, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return BASE_URL + "/mytrux/bookingtrack/booking/" + enCodedBookingNo + "/vehicle/" + vehicleRegNo;
    }

    public static String VEHICLELISTAPI(String bookingNo) {
        String enCodedBookingNo = null;
        try {
            enCodedBookingNo = URLEncoder.encode(bookingNo, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return BASE_URL + "/mytrux/booking/" + enCodedBookingNo + "/vehicles/";
    }*/
}
