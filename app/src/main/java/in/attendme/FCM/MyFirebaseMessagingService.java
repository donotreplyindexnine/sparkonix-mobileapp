package in.attendme.FCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import in.attendme.Model.FcmResponse;
import in.attendme.NotificationMessage;
import in.attendme.R;
import in.attendme.Utils.Constants;
import in.attendme.Utils.DatabaseHandler;

/**
 * Created by sagar on 10/13/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    //DatabaseHandler db = new DatabaseHandler(this);
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Message: " + remoteMessage.getNotification());
        DatabaseHandler db = new DatabaseHandler(this);

       // Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        FcmResponse fcmResponse  = new FcmResponse();
        /*fcmResponse.setTitle("AttendMe");
        fcmResponse.setMessage(remoteMessage.getNotification().getBody());

        db.addNotification(fcmResponse);*/

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            FcmResponse fcmResponse1  = new FcmResponse();
            fcmResponse1.setTitle(remoteMessage.getData().get("title"));

            Log.d(TAG, "Title: "+ remoteMessage.getData().get("title"));

            fcmResponse1.setMessage(remoteMessage.getData().get("message"));

            Log.d(TAG, "Message: "+ remoteMessage.getData().get("message"));

            fcmResponse1.setDescription(remoteMessage.getData().get("description"));

            Log.d(TAG, "Description: "+ remoteMessage.getData().get("description"));

            db.addComplaintRegistration(fcmResponse1);

            sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), remoteMessage.getData().get("description"));

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //sendNotification(remoteMessage.getNotification().getBody());

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *  @param title
     * @param messageBody FCM message body received.
     * @param description
     */
    private void sendNotification(String title, String messageBody, String description) {
        Intent intent = new Intent(this, NotificationMessage.class);
        intent.putExtra("title", title);
        intent.putExtra("message", messageBody);
        intent.putExtra("description", description);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        this,
                        0 /* Request code */,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(324 /* ID of notification */, notificationBuilder.build());

        sendBroadcast(Constants.SHOW_NOTIFICATION);
    }


    private void sendBroadcast(String showNotification) {
        Intent intent = new Intent();
        intent.setAction(showNotification);
        sendBroadcast(intent);

    }
}
