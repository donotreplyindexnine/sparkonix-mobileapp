package in.attendme.FCM;

import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import in.attendme.Model.FcmTokenDTO;
import in.attendme.Model.FcmTokenUpdate;
import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

/**
 * Created by sagar on 10/13/2016.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;




    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        editor.putString(Constants.FCM_ID, refreshedToken).apply();

        Log.d(TAG, sharedPreferences.getString(Constants.FCM_ID, ""));
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        String Token = sharedPreferences.getString(Constants.TOKEN,null);
        if (Token != null)
        {
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

            FcmTokenUpdate fcmTokenUpdate = new FcmTokenUpdate();
            fcmTokenUpdate.setFcmToken(token);

            Gson gson = new Gson();
            String data = gson.toJson(fcmTokenUpdate);

            Log.d(TAG,data);

            GenericRequest<FcmTokenDTO> UpdateFcmId = new GenericRequest<FcmTokenDTO>(Request.Method.PUT, APIUrls.UpdateFcmToken(), FcmTokenDTO.class, fcmTokenUpdate, new Response.Listener<FcmTokenDTO>() {
                @Override
                public void onResponse(FcmTokenDTO response) {
                    Log.d(TAG,response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG,error.toString());
                }
            },APIUrls.getDefaultHeaders("authorization",sharedPreferences.getString(Constants.TOKEN,null)));

            queue.add(UpdateFcmId);
        }


    }
}
