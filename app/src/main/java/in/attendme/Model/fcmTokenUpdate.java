package in.attendme.Model;

/**
 * Created by sagar on 12/2/2016.
 */

public class FcmTokenUpdate {
    private String fcmToken;

    public FcmTokenUpdate() {
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
