package in.attendme.Model;

/**
 * Created by sagar on 9/28/2016.
 */

public class RegisterComplaintRequest {
    private Long machineId;
    private String details;

    public Long getMachineId() {
        return machineId;
    }

    public void setMachineId(Long machineId) {
        this.machineId = machineId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
