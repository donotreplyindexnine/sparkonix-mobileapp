package in.attendme.Model;

/**
 * Created by sagar on 12/5/2016.
 */

public class FcmTokenDTO {

    private String fcmToken;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
