package in.attendme.Model;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by sagar on 9/22/2016.
 */

public class MachineInformationResponse {
   private String serialNumber;
    private String curAmcType;
    private String curAmcStatus;
    private String modelNumber;
    private long installationDate;
    private long lastServiceDate;
    private long curAmcStartDate;
    private long curAmcEndDate;
    private long machineId;
    private Manufacturer manufacturer;


    public long getMachineId() {
        return machineId;
    }

    public void setMachineId(long machineId) {
        this.machineId = machineId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCurAmcType() {
        return curAmcType;
    }

    public void setCurAmcType(String curAmcType) {
        this.curAmcType = curAmcType;
    }

    public String getCurAmcStatus() {
        return curAmcStatus;
    }

    public void setCurAmcStatus(String curAmcStatus) {
        this.curAmcStatus = curAmcStatus;
    }

    public long getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(long installationDate) {
        this.installationDate = installationDate;
    }

    public long getLastServiceDate() {
        return lastServiceDate;
    }

    public void setLastServiceDate(long lastServiceDate) {
        this.lastServiceDate = lastServiceDate;
    }

    public long getCurAmcStartDate() {
        return curAmcStartDate;
    }

    public void setCurAmcStartDate(long curAmcStartDate) {
        this.curAmcStartDate = curAmcStartDate;
    }

    public long getCurAmcEndDate() {
        return curAmcEndDate;
    }

    public void setCurAmcEndDate(long curAmcEndDate) {
        this.curAmcEndDate = curAmcEndDate;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getDate(long time) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;

        /*Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time*1000L);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;*/
    }

    public static class Manufacturer {
        private int id;
        private int onBoardedBy;
        private String companyName;
        private String custSupportName;
        private String custSupportPhone;
        private String custSupportEmail;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getOnBoardedBy() {
            return onBoardedBy;
        }

        public void setOnBoardedBy(int onBoardedBy) {
            this.onBoardedBy = onBoardedBy;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCustSupportName() {
            return custSupportName;
        }

        public void setCustSupportName(String custSupportName) {
            this.custSupportName = custSupportName;
        }

        public String getCustSupportPhone() {
            return custSupportPhone;
        }

        public void setCustSupportPhone(String custSupportPhone) {
            this.custSupportPhone = custSupportPhone;
        }

        public String getCustSupportEmail() {
            return custSupportEmail;
        }

        public void setCustSupportEmail(String custSupportEmail) {
            this.custSupportEmail = custSupportEmail;
        }
    }
}
