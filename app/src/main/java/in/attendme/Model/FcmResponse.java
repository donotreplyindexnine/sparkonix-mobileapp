package in.attendme.Model;

/**
 * Created by sagar on 10/13/2016.
 */

public class FcmResponse {
    int _id;
    String title;
    String message;
    String description;


    public FcmResponse() {
    }

    public FcmResponse(int _id, String title, String message, String description) {
        this.title = title;
        this._id = _id;
        this.message = message;
        this.description = description;
    }

    public FcmResponse(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public FcmResponse(String message) {
        this.message = message;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
