package in.attendme.Model;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by sagar on 9/28/2016.
 */

public class IssueDetails {
    private String issueNumber;
    private String machineSerialNumber;
    private String manufacturerName;
    private String complaintDetail;
    private String operatorName;
    private String operatorContactNumber;
    private String attendedBy;
    private String attendedByContactNumber;
    private long loggedDate;

    public String getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getMachineSerialNumber() {
        return machineSerialNumber;
    }

    public void setMachineSerialNumber(String machineSerialNumber) {
        this.machineSerialNumber = machineSerialNumber;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getComplaintDetail() {
        return complaintDetail;
    }

    public void setComplaintDetail(String complaintDetail) {
        this.complaintDetail = complaintDetail;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorContactNumber() {
        return operatorContactNumber;
    }

    public void setOperatorContactNumber(String operatorContactNumber) {
        this.operatorContactNumber = operatorContactNumber;
    }

    public String getAttendedBy() {
        return attendedBy;
    }

    public void setAttendedBy(String attendedBy) {
        this.attendedBy = attendedBy;
    }

    public String getAttendedByContactNumber() {
        return attendedByContactNumber;
    }

    public void setAttendedByContactNumber(String attendedByContactNumber) {
        this.attendedByContactNumber = attendedByContactNumber;
    }

    public long getLoggedDate() {
        return loggedDate;
    }

    public void setLoggedDate(long loggedDate) {
        this.loggedDate = loggedDate;
    }

    public String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }
}
