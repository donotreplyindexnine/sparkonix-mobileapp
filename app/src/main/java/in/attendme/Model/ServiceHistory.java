package in.attendme.Model;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by sagar on 12/9/2016.
 */

public class ServiceHistory {

    private long id, machineId, companyId;
    private String details, assignedToName, assignedToEmail, assignedToMobile, actionTaken;
    private long servicingAssignedDate, servicingDoneDate;

    public ServiceHistory() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMachineId() {
        return machineId;
    }

    public void setMachineId(long machineId) {
        this.machineId = machineId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAssignedToName() {
        return assignedToName;
    }

    public void setAssignedToName(String assignedToName) {
        this.assignedToName = assignedToName;
    }

    public String getAssignedToEmail() {
        return assignedToEmail;
    }

    public void setAssignedToEmail(String assignedToEmail) {
        this.assignedToEmail = assignedToEmail;
    }

    public String getAssignedToMobile() {
        return assignedToMobile;
    }

    public void setAssignedToMobile(String assignedToMobile) {
        this.assignedToMobile = assignedToMobile;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public long getServicingAssignedDate() {
        return servicingAssignedDate;
    }

    public void setServicingAssignedDate(long servicingAssignedDate) {
        this.servicingAssignedDate = servicingAssignedDate;
    }

    public long getServicingDoneDate() {
        return servicingDoneDate;
    }

    public void setServicingDoneDate(long servicingDoneDate) {
        this.servicingDoneDate = servicingDoneDate;
    }

    public static String toCalendar(long time){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceHistory that = (ServiceHistory) o;

        return getId() == that.getId();

    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }
}
