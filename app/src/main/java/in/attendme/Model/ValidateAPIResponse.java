package in.attendme.Model;

/**
 * Created by sagar on 9/21/2016.
 */

public class ValidateAPIResponse {

    private String success;
    private String token;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
