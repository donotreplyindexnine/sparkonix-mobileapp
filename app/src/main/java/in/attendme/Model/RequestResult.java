package in.attendme.Model;

/**
 * Created by sagar on 12/7/2016.
 */

public class RequestResult {
    private String success;
    private String error;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
