package in.attendme.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import in.attendme.Model.FcmResponse;
import in.attendme.NotificationMessage;
import in.attendme.R;

/**
 * Created by sagar on 10/13/2016.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private TextView Notification_row, Notification_message;
    private List<FcmResponse> fcmResponses;
    private Activity activity;

    public NotificationAdapter(List<FcmResponse> fcmResponses, Activity activity) {
        this.fcmResponses = fcmResponses;
        this.activity = activity;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_row, viewGroup, false);
        Notification_row = (TextView) v.findViewById(R.id.notification_title);
        Notification_message = (TextView) v.findViewById(R.id.notification_message);
        return new ViewHolder(v, Notification_row, Notification_message);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {
        holder.Notification_row.setText(fcmResponses.get(position).getTitle());
        holder.Notification_message.setText(fcmResponses.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return fcmResponses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView Notification_row, Notification_message;

        public ViewHolder(View item, TextView notification_row, TextView notification_message) {
            super(item);
            Notification_row = notification_row;
            Notification_message = notification_message;
            item.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            //String status = fcmResponses.get(position).getMessage();
            Intent intent = new Intent(activity,NotificationMessage.class);
            intent.putExtra("title", fcmResponses.get(position).getTitle());
            intent.putExtra("message", fcmResponses.get(position).getMessage());
            intent.putExtra("description", fcmResponses.get(position).getDescription());
            activity.startActivity(intent);
        }
    }

    public void changeList(List<FcmResponse> fcmResponses) {

        if (fcmResponses != null && fcmResponses.size() > 0) {
            this.fcmResponses.clear();
            this.fcmResponses.addAll(fcmResponses);
            Collections.reverse(this.fcmResponses);
            notifyDataSetChanged();
        }

    }
}
