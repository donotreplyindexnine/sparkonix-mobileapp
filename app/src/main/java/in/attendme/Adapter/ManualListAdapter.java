package in.attendme.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;

import java.util.ArrayList;

import in.attendme.ManualList;
import in.attendme.Model.MachineManualList;
import in.attendme.R;

/**
 * Created by sagar on 12/9/2016.
 */
public class ManualListAdapter extends RecyclerView.Adapter<ManualListAdapter.ViewHolder>{

    private TextView Manuals, ManualsName;
    private ArrayList<MachineManualList> manualLists;
    private Activity activity;

    public ManualListAdapter(ArrayList<MachineManualList> manualLists, ManualList activity) {
        this.manualLists = manualLists;
        this.activity = activity;
    }


    @Override
    public ManualListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manual_list_row, viewGroup, false);
        Manuals = (TextView) v.findViewById(R.id.Manuals);
        ManualsName = (TextView) v.findViewById(R.id.ManualsName);
        return new ManualListAdapter.ViewHolder(v, Manuals, ManualsName);
    }

    @Override
    public void onBindViewHolder(ManualListAdapter.ViewHolder holder, int position) {
        holder.ManualsName.setText(URLUtil.guessFileName(manualLists.get(position).getDocumentPath(), null, null));
        holder.Manuals.setText(manualLists.get(position).getDescription());

    }

    @Override
    public int getItemCount() {
        return manualLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView Manuals, ManualsName;

        public ViewHolder(View itemView, TextView manuals, TextView manualsName) {
            super(itemView);
            Manuals = manuals;
            ManualsName = manualsName;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            String extension = (manualLists.get(position).getDocumentPath()).substring((manualLists.get(position).getDocumentPath()).lastIndexOf(".") + 1);
            if (extension.equals("jpg") || extension.equals("png") || extension.equals("jpeg") )
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,  Uri.parse( manualLists.get(position).getDocumentPath() ));
                activity.startActivity(browserIntent);
            }
            else if (extension.equals("pdf") || extension.equals("doc") || extension.equals("docx")|| extension.equals("txt"))
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://docs.google.com/gview?embedded=true&url="+manualLists.get(position).getDocumentPath()));
                activity.startActivity(browserIntent);
            }

        }
    }
}
