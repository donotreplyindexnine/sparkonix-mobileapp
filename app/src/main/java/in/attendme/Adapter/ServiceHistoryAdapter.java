package in.attendme.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.attendme.FullServicingInfo;
import in.attendme.Model.ServiceHistory;
import in.attendme.R;

/**
 * Created by sagar on 12/9/2016.
 */

public class ServiceHistoryAdapter extends RecyclerView.Adapter<ServiceHistoryAdapter.ViewHolder> {

    private TextView Service_date;
    private List<ServiceHistory> serviceHistoryList;
    private Activity activity;

    public ServiceHistoryAdapter(List<ServiceHistory> serviceHistoryList, Activity activity) {
        this.serviceHistoryList = serviceHistoryList;
        this.activity = activity;
    }

    @Override
    public ServiceHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.complaint_history_row, viewGroup, false);
        /*ServicedOn = (TextView) v.findViewById(R.id.servicedOn);*/
        Service_date = (TextView) v.findViewById(R.id.service_date);
        return new ServiceHistoryAdapter.ViewHolder(v, Service_date);
    }

    @Override
    public void onBindViewHolder(ServiceHistoryAdapter.ViewHolder holder, int position) {

        holder.Service_date.setText(ServiceHistory.toCalendar(serviceHistoryList.get(position).getServicingDoneDate()));

    }

    @Override
    public int getItemCount() {
        return serviceHistoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView Service_date;


        public ViewHolder(View v, TextView service_date) {
            super(v);
            /*ServicedOn = servicedOn;*/
            Service_date = service_date;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int position = getLayoutPosition();
            //String status = fcmResponses.get(position).getMessage();
            Intent intent = new Intent(activity,FullServicingInfo.class);
            intent.putExtra("ServicingDetails", serviceHistoryList.get(position).getDetails());

            intent.putExtra("ServicingAssignedDate",
                    ServiceHistory.toCalendar(serviceHistoryList.get(position).getServicingAssignedDate()));

            intent.putExtra("ServicingDoneDate",
                    ServiceHistory.toCalendar(serviceHistoryList.get(position).getServicingDoneDate()));

            intent.putExtra("TechnicianName", serviceHistoryList.get(position).getAssignedToName());
            intent.putExtra("TechnicianEmail", serviceHistoryList.get(position).getAssignedToEmail());
            intent.putExtra("TechnicianMobileNumber", serviceHistoryList.get(position).getAssignedToMobile());
            intent.putExtra("ActionTaken", serviceHistoryList.get(position).getActionTaken());

            activity.startActivity(intent);

        }
    }
}
