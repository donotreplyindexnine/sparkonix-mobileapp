package in.attendme.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import in.attendme.FullIndividualComplaint;
import in.attendme.Model.ComplaintHistoryListResponse;
import in.attendme.Model.IssueDetails;
import in.attendme.R;
import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

/**
 * Created by sagar on 9/26/2016.
 */

public class ComplaintHistoryAdapter extends RecyclerView.Adapter<ComplaintHistoryAdapter.ViewHolder> {

    private TextView ComplaintID, ComplaintDate,Status,ComplaintDescription;
    private View Coloring;
    private ArrayList<ComplaintHistoryListResponse> historyListResponses;
    private Activity activity;
    private SharedPreferences sharedPreferences;

    public ComplaintHistoryAdapter(ArrayList<ComplaintHistoryListResponse> historyListResponses, Activity activity) {
        this.historyListResponses = historyListResponses;
        this.activity = activity;
        sharedPreferences = activity.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public ComplaintHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.complaint_row, viewGroup, false);
        ComplaintID = (TextView) v.findViewById(R.id.ComplaintID);
        ComplaintDate = (TextView) v.findViewById(R.id.ComplaintDate);
        Status = (TextView) v.findViewById(R.id.Status);
        ComplaintDescription = (TextView) v.findViewById(R.id.ComplaintDescription);
        Coloring = v.findViewById(R.id.Color);
        return new ViewHolder(v,ComplaintID,ComplaintDate,Status,ComplaintDescription, Coloring);
    }

    @Override
    public void onBindViewHolder(ComplaintHistoryAdapter.ViewHolder holder, int position) {
        holder.ComplaintID.setText(historyListResponses.get(position).getIssueNumber());
        if (historyListResponses.get(position).getDateReported()!= 0)
        {
            holder.ComplaintDate.setText(historyListResponses.get(position).getDate(historyListResponses.get(position).getDateReported()));
        }
        else
        {
            holder.ComplaintDate.setText("");
        }

        if ((historyListResponses.get(position).getStatus()).equals("OPEN"))
        {
            holder.Status.setTextColor(activity.getResources().getColor(R.color.RedColor));
            holder.Coloring.setBackgroundColor(activity.getResources().getColor(R.color.RedColor));

        }
        else if((historyListResponses.get(position).getStatus()).equals("CLOSED"))
        {
            holder.Status.setTextColor(activity.getResources().getColor(R.color.FaintGreen));
            holder.Coloring.setBackgroundColor(activity.getResources().getColor(R.color.FaintGreen));

        }
        else if ((historyListResponses.get(position).getStatus()).equals("ASSIGNED"))
        {
            holder.Status.setTextColor(activity.getResources().getColor(R.color.Violet));
            holder.Coloring.setBackgroundColor(activity.getResources().getColor(R.color.Violet));

        }
        holder.Status.setText(historyListResponses.get(position).getStatus());
        holder.ComplaintDescription.setText(historyListResponses.get(position).getDetails());

    }

    @Override
    public int getItemCount() {
        return historyListResponses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView ComplaintID, ComplaintDate,Status,ComplaintDescription;
        View Coloring;
        private ProgressDialog progress;

        ViewHolder(View itemView, TextView complaintID, TextView complaintDate, TextView status, TextView complaintDescription, View color) {
            super(itemView);
            ComplaintID = complaintID;
            ComplaintDate = complaintDate;
            Status = status;
            ComplaintDescription = complaintDescription;
            Coloring = color;
            progress = new ProgressDialog(activity);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            Long aLong  = historyListResponses.get(position).getId();
            final String status = historyListResponses.get(position).getStatus();

            RequestQueue queue = Volley.newRequestQueue(activity);
            progress.setMessage("Wait while loading...");
            progress.setCancelable(false);
            progress.show();
            GenericRequest<IssueDetails> genericRequest = new GenericRequest<>(Request.Method.GET, APIUrls.IndividualComplaintFullDetail(aLong), IssueDetails.class, null, new Response.Listener<IssueDetails>() {
                @Override
                public void onResponse(IssueDetails response) {

                    if (progress.isShowing())
                    {
                        progress.dismiss();
                    }

                    Intent intent = new Intent(activity, FullIndividualComplaint.class);
                    intent.putExtra("status", status);
                    intent.putExtra("issueNumber", response.getIssueNumber());
                    intent.putExtra("machineSerialNumber",response.getMachineSerialNumber());
                    intent.putExtra("manufacturerName", response.getManufacturerName());
                    intent.putExtra("complaintDetail",response.getComplaintDetail());
                    intent.putExtra("fragment_state", 1);
                    if (response.getLoggedDate() != 0)
                    {
                        intent.putExtra("loggedDate",response.getDate(response.getLoggedDate()));
                    }
                    else
                    {
                        intent.putExtra("loggedDate","");
                    }

                    intent.putExtra("operatorName", response.getOperatorName());
                    intent.putExtra("operatorContactNumber",response.getOperatorContactNumber());
                    intent.putExtra("attendedBy",response.getAttendedBy());
                    intent.putExtra("attendedByContactNumber", response.getAttendedByContactNumber());
                    activity.startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (progress.isShowing())
                    {
                        progress.dismiss();
                    }
                    showErrorDialog();
                }
            },APIUrls.getDefaultHeaders("authorization",sharedPreferences.getString(Constants.TOKEN,null)));
            queue.add(genericRequest);
        }

        private void showErrorDialog()
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage("Details not avialable")
                    .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }
}
