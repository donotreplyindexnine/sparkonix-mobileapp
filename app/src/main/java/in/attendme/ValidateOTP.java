package in.attendme;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import in.attendme.Model.ValidateAPIRequest;
import in.attendme.Model.ValidateAPIResponse;
import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;
import in.attendme.Volley.GenericRequest;

public class ValidateOTP extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String TAG = "ValidateOTP";
    private Button ValidateButton;
    private EditText OTP;
    //private TextView Message;
    //private TextView Status;
    private String Mobile_Number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_otp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle(R.string.app_name);
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Mobile_Number = this.getIntent().getExtras().getString("Mobile_Number");
        ValidateButton = (Button) findViewById(R.id.ButtonValidate);
        OTP = (EditText) findViewById(R.id.OTPEditText);
        OTP.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        OTP.setFocusable(true);
        OTP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    hideKeyboard(view);
                }
            }
        });
        //Message = (TextView) findViewById(R.id.Message);
        //Message.setText(getString(R.string.OTPTextView));
        //Status = (TextView) findViewById(R.id.Status);

        ValidateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (OTP.getText().toString().equals(""))
                {
                    showDialog("OTP");
                }
                else
                {
                    validateAPI();
                }
            }
        });

    }

    private void validateAPI() {
        RequestQueue queue = Volley.newRequestQueue(ValidateOTP.this);

        String otp = OTP.getText().toString();

        ValidateAPIRequest validateAPIRequest = new ValidateAPIRequest();
        validateAPIRequest.setPhoneNumber(Mobile_Number);
        validateAPIRequest.setOtp(otp);
        validateAPIRequest.setFcmToken(sharedPreferences.getString(Constants.FCM_ID,""));

        Gson gson = new Gson();
        String data = gson.toJson(validateAPIRequest);

        Log.d(TAG,data);
        //Log.d(TAG,APIUrls.getDefaultHeaders("authorization", sharedPreferences.getString(Constants.TOKEN, null)).toString());

        final ProgressDialog progress = new ProgressDialog(this);
        //progress.setTitle("Loading");
        progress.setMessage(getString(R.string.wait));
        progress.setCancelable(false);
        progress.show();

        GenericRequest<ValidateAPIResponse> IsValidOTP = new GenericRequest<>(Request.Method.POST, APIUrls.ValidateOTP(), ValidateAPIResponse.class, validateAPIRequest, new Response.Listener<ValidateAPIResponse>() {
            @Override
            public void onResponse(ValidateAPIResponse response) {

                if (progress.isShowing())
                {
                    progress.dismiss();
                }

                editor.putString(Constants.TOKEN,response.getToken()).apply();
                editor.putBoolean(Constants.IsApproved, true).apply();
                Intent intent = new Intent(ValidateOTP.this, QRcodeReadingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progress.isShowing())
                {
                    progress.dismiss();
                }

                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showNetworkErrorDialog(getString(R.string.No_Internet));
                }
                else
                {
                    showErrorDialog();
                }


                Log.d(TAG,error.toString());
            }
        },null);

        queue.add(IsValidOTP);
    }

    private void showNetworkErrorDialog(String string) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(string)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void showDialog(String field)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enter "+ field)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();


        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    private void showErrorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("OTP Is Invalid")
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AquaGreen));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        else if (id == R.id.action_notifications) {
            Intent intent = new Intent(this,NotificationList.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    private BroadcastReceiver broadcastReceiver = new IncomingSms();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);

    }

    public class IncomingSms extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String Sms_Otp = "";
            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (int i = 0; i < pdusObj.length; i++) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String senderNum = phoneNumber;
                        String message = currentMessage.getDisplayMessageBody();

                        String[] messageSplit = message.split(getString(R.string.Read_sms));
                        if (messageSplit[1] != null) {

                            Sms_Otp = messageSplit[1].trim();
                           /* String m = messageSplit[1];
                            String[] otpSplit = m.split(getString(R.string.as_otp_to_log_into_healthpole));
                            if (otpSplit[0] != null) {

                            }*/
                        }
                        try {
                            //LoginActivity.this.hideProgressBar();
                            ValidateOTP.this.OTP.setText(Sms_Otp);
                            //LoginActivity.this.loginBtn.performClick();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
