package in.attendme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import in.attendme.Utils.Constants;

public class FullServicingInfo extends AppCompatActivity {

    private TextView ServicingDetailsTextView, ServicingAssignedDateTextView , ServicingDoneDateTextView,
            TechnicianNameTextView, TechnicianEmailTextView,
            TechnicianMobileNumberTextView, ActionTakenTextView;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_servicing_info);

        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.ServicingDetails));

        ServicingDetailsTextView = (TextView) findViewById(R.id.ServicingDetailsTextView);
        ServicingAssignedDateTextView = (TextView) findViewById(R.id.ServicingAssignedDateTextView);
        ServicingDoneDateTextView = (TextView) findViewById(R.id.ServicingDoneDateTextView);
        TechnicianNameTextView = (TextView) findViewById(R.id.TechnicianNameTextView);
        TechnicianEmailTextView = (TextView) findViewById(R.id.TechnicianEmailTextView);
        TechnicianMobileNumberTextView = (TextView) findViewById(R.id.TechnicianMobileNumberTextView);
        ActionTakenTextView = (TextView) findViewById(R.id.ActionTakenTextView);


        ServicingDetailsTextView.setText(getIntent().getExtras().getString("ServicingDetails"));
        ServicingAssignedDateTextView.setText(getIntent().getExtras().getString("ServicingAssignedDate"));
        ServicingDoneDateTextView.setText(getIntent().getExtras().getString("ServicingDoneDate"));
        TechnicianNameTextView.setText(getIntent().getExtras().getString("TechnicianName"));
        TechnicianEmailTextView.setText(getIntent().getExtras().getString("TechnicianEmail"));
        TechnicianMobileNumberTextView.setText(getIntent().getExtras().getString("TechnicianMobileNumber"));
        ActionTakenTextView.setText(getIntent().getExtras().getString("ActionTaken"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            /*if (getIntent().getExtras()!= null)
            {
                editor.putInt(Constants.WhichFragment, getIntent().getExtras().getInt("fragment_state")).apply();
            }*/
            onBackPressed();
        }

        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }

       /* else if (id == R.id.action_notifications) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

}
