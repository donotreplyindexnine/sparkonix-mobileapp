package in.attendme.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.attendme.Model.FcmResponse;

/**
 * Created by sagar on 10/13/2016.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";

    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "NotificationManager";

    // Contacts table name



    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(complaints.CREATE_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //db.execSQL("DROP TABLE IF EXISTS " + complaints.TABLE_NAME);

        if (newVersion > oldVersion)
        {
            db.execSQL("ALTER TABLE " + complaints.TABLE_NAME +" ADD COLUMN "+ complaints.READ +" INTEGER NOT NULL DEFAULT 0 ");
        }

        onCreate(db);

    }

    //insert complaint registration
    public void addComplaintRegistration(FcmResponse fcmResponse) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(complaints.KEY_TITLE, fcmResponse.getTitle()); // Notification title
        values.put(complaints.MESSAGE, fcmResponse.getMessage()); // Notification message
        values.put(complaints.DESCRIPTION, fcmResponse.getDescription()); // Notification Long Description
        values.put(complaints.CREATED_AT, getCurrentDate()); // date of registration

        Log.d(TAG, fcmResponse.getTitle());
        Log.d(TAG, fcmResponse.getMessage());
        Log.d(TAG, fcmResponse.getDescription());

        // Inserting Row
        db.insert(complaints.TABLE_NAME, complaints.COLUMN_NAME_NULLABLE, values);
        db.close(); // Closing database connection
    }

    /*//insert complaint assigned info
    public void addComplaintAssignedInfo(FcmResponse fcmResponse) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(complaintAssigned.KEY_TITLE, fcmResponse.getTitle()); // Notification title
        values.put(complaintAssigned.MESSAGE, fcmResponse.getMessage()); // Notification message
        values.put(complaintAssigned.DESCRIPTION, fcmResponse.getDescription()); // Notification Long Description
        values.put(complaintAssigned.CREATED_AT, getCurrentDate()); // date of registration

        Log.d(TAG, fcmResponse.getTitle());
        Log.d(TAG, fcmResponse.getMessage());
        Log.d(TAG, fcmResponse.getDescription());
        Log.d(TAG, fcmResponse.getDescription());

        // Inserting Row
        db.insert(complaintAssigned.TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }*/

    //insert complaint resolved info
    /*public void addComplaintResolvedInfo(FcmResponse fcmResponse) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(complaintResolved.KEY_TITLE, fcmResponse.getTitle()); // Notification title
        values.put(complaintResolved.MESSAGE, fcmResponse.getMessage()); // Notification message
        values.put(complaintResolved.DESCRIPTION, fcmResponse.getDescription()); // Notification Long Description
        values.put(complaintResolved.CREATED_AT, getCurrentDate()); // date of registration

        Log.d(TAG, fcmResponse.getTitle());
        Log.d(TAG, fcmResponse.getMessage());
        Log.d(TAG, fcmResponse.getDescription());
        Log.d(TAG, fcmResponse.getDescription());

        // Inserting Row
        db.insert(complaintResolved.TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }*/
    //Get single notification
    FcmResponse getSingleNotification(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(complaints.TABLE_NAME, new String[] { complaints.KEY_ID,
                        complaints.KEY_TITLE, complaints.MESSAGE }, complaints.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        FcmResponse fcmResponse = new FcmResponse(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        // return contact
        return fcmResponse;
    }

    // Getting All Contacts
    public List<FcmResponse> getAllNotification() {
        List<FcmResponse> fcmResponseList = new ArrayList<FcmResponse>();
        // Select All Query
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(complaints.TABLE_NAME, null, null,
                null, null, null, complaints.CREATED_AT, null);
        if (cursor != null)
            cursor.moveToFirst();

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do {
                FcmResponse fcmResponse = new FcmResponse();
                fcmResponse.set_id(Integer.parseInt(cursor.getString(0)));
                fcmResponse.setTitle(cursor.getString(1));
                fcmResponse.setMessage(cursor.getString(2));
                fcmResponse.setDescription(cursor.getString(3));
                // Adding contact to list
                fcmResponseList.add(fcmResponse);
            } while (cursor.moveToNext());
        }



        cursor.close();

        // return contact list
        return fcmResponseList;
    }

    public int getUnreadNotificationCount()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(complaints.TABLE_NAME, new String[] {complaints.READ}, complaints.READ + "=?",
                new String[] { 0 + "" }, null, null, null, null);

        return cursor.getCount();
    }

    public long updateUnreadNotificationColumn()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        try {

            ContentValues values = new ContentValues();
            values.put(complaints.READ, 1);
            long check = db.update(complaints.TABLE_NAME, values, complaints.READ + "=?", new String[] {"" + 0} );
            return check;
        }
        finally {

        }


    }


    /*// Updating single notification
    public int updateNotification(FcmResponse fcmResponse) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, fcmResponse.getTitle());
        values.put(MESSAGE, fcmResponse.getMessage());

        // updating row
        return db.update(TABLE_NOTIFICATION, values, KEY_ID + " = ?",
                new String[] { String.valueOf(fcmResponse.get_id()) });
    }*/

    /*// Deleting single notification
    public void deleteNotification(FcmResponse fcmResponse) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATION, KEY_ID + " = ?",
                new String[] { String.valueOf(fcmResponse.get_id()) });
        db.close();
    }*/


    // Getting contacts Count
    /*public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NOTIFICATION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }*/

    //Insert complaint record
    public static class complaints {
        public static final String TABLE_NAME = "complaint_registered";
        private static final String KEY_ID = "id";
        private static final String KEY_TITLE = "title";
        private static final String MESSAGE = "message";
        private static final String DESCRIPTION = "description";
        private static final String CREATED_AT = "created_at";
        private static final String READ = "read";


        // Contacts Table Columns names
        public static final String CREATE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + complaints.TABLE_NAME +
                " ( " + complaints.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                complaints.KEY_TITLE + " TEXT , " +
                complaints.MESSAGE + " TEXT , " +
                complaints.DESCRIPTION + " TEXT , " +
                complaints.READ + " INTEGER NOT NULL DEFAULT 0 , "+
                complaints.CREATED_AT + " TEXT );";

        public static final String COLUMN_NAME_NULLABLE = null;
    }


    //Insert complaint assigned info
    /*public static class complaintAssigned {
        public static final String TABLE_NAME = "complaint_assigned";
        private static final String KEY_ID = "id";
        private static final String KEY_TITLE = "title";
        private static final String MESSAGE = "message";
        private static final String DESCRIPTION = "description";
        private static final String CREATED_AT = "created_at";


        // Contacts Table Columns names
        public static final String CREATE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + complaintAssigned.TABLE_NAME +
                " ( " + complaintAssigned.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                complaintAssigned.KEY_TITLE + " TEXT , " +
                complaintAssigned.MESSAGE + " TEXT , " +
                complaintAssigned.DESCRIPTION + " TEXT , " +
                complaintAssigned.CREATED_AT + " TEXT);";

        public static final String COLUMN_NAME_NULLABLE = null;
    }

    //Insert complaint resolved info
    public static class complaintResolved {
        public static final String TABLE_NAME = "complaint_resolved";
        private static final String KEY_ID = "id";
        private static final String KEY_TITLE = "title";
        private static final String MESSAGE = "message";
        private static final String DESCRIPTION = "description";
        private static final String CREATED_AT = "created_at";


        // Contacts Table Columns names
        public static final String CREATE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS " + complaintResolved.TABLE_NAME +
                " ( " + complaintResolved.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                complaintResolved.KEY_TITLE + " TEXT , " +
                complaintResolved.MESSAGE + " TEXT , " +
                complaintResolved.DESCRIPTION + " TEXT , " +
                complaintResolved.CREATED_AT + " TEXT);";

        public static final String COLUMN_NAME_NULLABLE = null;
    }*/


    public String getCurrentDate() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        return sdf.format(c.getTime());

    }

}
