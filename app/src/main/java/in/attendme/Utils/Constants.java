package in.attendme.Utils;

/**
 * Created by sagar on 9/21/2016.
 */

public class Constants {
    public static final String PREFERENCE_NAME = "preference_name";
    public static final String TOKEN = "token";
    public static final String MANUFACTURE_ID = "manufacture_id";
    public static final String MACHINE_ID = "machine_id";
    public static final String IS_FIRST_TIME = "is_first_time";
    public static final String IsApproved = "is_approved";
    public static final String WhichFragment = "whichFragment";
    public static final String FCM_ID = "fcm_id";
    public static final String MODEL_NUMBER = "model_number";

    public static final String SHOW_NOTIFICATION = "show_notification";
}
