package in.attendme;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import in.attendme.Utils.Constants;

public class VersionScreen extends Activity {

    private TextView VersionNameTextView;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_screen);
        VersionNameTextView = (TextView) findViewById(R.id.VersionNameTextView);
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        /*PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;*/

        VersionNameTextView.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getExtras()!= null)
        {
            editor.putInt(Constants.WhichFragment, getIntent().getExtras().getInt("fragment_state")).apply();
        }

        super.onBackPressed();
    }
}
