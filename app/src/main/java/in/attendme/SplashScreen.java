package in.attendme;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import in.attendme.Utils.Constants;

public class SplashScreen extends Activity {

    private static final String TAG = "SplashScreen";
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        /*if (getIntent().getExtras() != null) {
            *//*for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }*//*
            DatabaseHandler db = new DatabaseHandler(this);
            FcmResponse fcmResponse1  = new FcmResponse();
            fcmResponse1.setTitle(getIntent().getExtras().getString("Title"));
            Log.d(TAG, "Title: "+ getIntent().getExtras().getString("Title"));
            fcmResponse1.setMessage(getIntent().getExtras().getString("Message"));
            Log.d(TAG, "Message: "+ getIntent().getExtras().getString("Message"));

            Gson gson = new Gson();
            String data = gson.toJson(fcmResponse1);
            Log.d(TAG,data);
            Toast.makeText(this, TAG+" "+data, Toast.LENGTH_SHORT).show();

            db.addNotification(fcmResponse1);

            Intent intent = new Intent(this, NotificationList.class);

            startActivity(intent);


        }*/
        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        setContentView(R.layout.activity_splash_screen);

    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Boolean IsApproved = sharedPreferences.getBoolean(Constants.IsApproved, false);
                if (!IsApproved)
                {
                    Intent mainIntent = new Intent(SplashScreen.this, TermsConditions.class);
                    SplashScreen.this.startActivity(mainIntent);
                    SplashScreen.this.finish();
                }
                else
                {
                    Intent intent = new Intent(SplashScreen.this, QRcodeReadingActivity.class);
                    startActivity(intent);
                    finish();


                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
