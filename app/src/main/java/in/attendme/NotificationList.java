package in.attendme;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.attendme.Adapter.NotificationAdapter;
import in.attendme.Model.FcmResponse;
import in.attendme.Utils.Constants;
import in.attendme.Utils.DatabaseHandler;

public class NotificationList extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private NotificationAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout refreshLayout;
    private List<FcmResponse> notificationList;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private TextView EmptyView;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.Notification));
        db = new DatabaseHandler(this);
        notificationList = new ArrayList<>();
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.NotificationList);
        mLayoutManager = new LinearLayoutManager(NotificationList.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new NotificationAdapter(notificationList, NotificationList.this);
        mRecyclerView.setAdapter(mAdapter);
        progressDialog = new ProgressDialog(this);
        EmptyView = (TextView) findViewById(R.id.EmptyView);

        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();
        //write Loading dialog here

        refreshLayout.setColorSchemeResources(
                R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark
        );

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EmptyView.setVisibility(View.GONE);
                getAllNotification();
            }
        });

        long value = db.updateUnreadNotificationColumn();

       // Toast.makeText(this, value + "Rows" + "updated" , Toast.LENGTH_SHORT).show();

        showProgressDialog();
        getAllNotification();

    }

    private void showProgressDialog() {
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void hideProgressDialog() {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void getAllNotification() {
        List<FcmResponse> newList = db.getAllNotification();

        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        hideProgressDialog();
        if (newList != null && newList.size() > 0) {
            mAdapter.changeList(newList);
        } else {
            EmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_machine_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        else if (id == R.id.about)
        {
            Intent intent = new Intent(this, VersionScreen.class);
            startActivity(intent);
            return true;
        }
       /* else if (id == R.id.action_notifications) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (isTaskRoot())
        {
            Intent intent = new Intent(this, QRcodeReadingActivity.class );
            startActivity(intent);
            finish();

        }
        else {

            if (getIntent().getExtras()!= null)
            {
                editor.putInt(Constants.WhichFragment, getIntent().getExtras().getInt("fragment_state")).apply();
            }

            super.onBackPressed();
        }
    }
}
