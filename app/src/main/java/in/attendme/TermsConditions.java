package in.attendme;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.attendme.Utils.Constants;
import in.attendme.Volley.APIUrls;

public class TermsConditions extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Button accept, decline;
    private TextView ReadMore, PrivacyPolicy, TermsAndConditions;
    private static String TAG = "TermsAndCondition";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        getSupportActionBar().setTitle(R.string.app_name);

        //termsConditions.setMovementMethod(new ScrollingMovementMethod());
        accept = (Button) findViewById(R.id.Accept);
        decline = (Button) findViewById(R.id.Decline);
        ReadMore = (TextView) findViewById(R.id.ReadMore);
        PrivacyPolicy = (TextView) findViewById(R.id.PrivacyPolicy);
        TermsAndConditions = (TextView) findViewById(R.id.Terms);

        ReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = APIUrls.TermsAndConditions();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        PrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = APIUrls.PrivacyPolicy();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        sharedPreferences = getSharedPreferences(Constants.PREFERENCE_NAME,MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean(Constants.IS_FIRST_TIME, true))
        {
            //TermsConditionsAPI();
            TermsAndConditions.setText(getString(R.string.Terms));
        }
        else
        {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(TermsConditions.this, MainActivity.class);
                startActivity(intent);
                editor.putBoolean(Constants.IS_FIRST_TIME,false).apply();
                finish();
            }
        });

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }

    /*private void TermsConditionsAPI() {
        RequestQueue queue = Volley.newRequestQueue(TermsConditions.this);

        *//*GenericRequest<String> genericRequest = new GenericRequest<String>(Request.Method.GET, APIUrls.TermsAndConditions(), String.class, null, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                termsConditions.setText(Html.fromHtml(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
*//*      final ProgressDialog progress = new ProgressDialog(this);
        //progress.setTitle("Loading");
        progress.setMessage(getString(R.string.wait));
        progress.setCancelable(false);
        progress.show();
        StringRequest genericRequest = new StringRequest(Request.Method.GET, APIUrls.TermsAndConditions(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (progress.isShowing())
                {
                    progress.dismiss();
                }
                Log.d(TAG, response);
                termsConditions.loadData(response, "text/html", "UTF-8");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progress.isShowing())
                {
                    progress.dismiss();
                }
                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                    showErrorDialog(getString(R.string.No_Internet));
                }
                else
                {
                    showErrorDialog(getString(R.string.No_Internet));
                }
                Log.d(TAG, error.toString());

            }
        });
        queue.add(genericRequest);
    }*/

    private void showErrorDialog(String ErrorMessage)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(ErrorMessage)
                .setNegativeButton(R.string.Okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
